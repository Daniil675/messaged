package main

import (
	"./server"
	"./storage/datastore"
	"./telegram"
	_ "./utility"
	"github.com/sirupsen/logrus"
	"sync"
	"upper.io/db.v3/postgresql"
)

//const (
//	listeningPort = "8080"
//)

func main() {
	logrus.Info("Wake up")
	settings := postgresql.ConnectionURL{
		User:     "",
		Password: "",
		Host:     "localhost",
		Database: "",
	}
	datastore.New(settings)
	//go analytics.Start()
	token := ""
	var wg sync.WaitGroup
	telegram.New(token)
	//
	wg.Add(1)
	go telegram.Start(&wg)
	s := &server.Server{}
	s.Start("80")
	wg.Wait()
}
