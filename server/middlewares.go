package server

import (
	"../storage/datastore"
	"github.com/alexedwards/stack"
	"net/http"
)

func userMiddleware(ctx *stack.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//fmt.Println(ps.ByName("name"), "visit site!")

		session := r.Header.Get("x-api-token")
		//utility.Logger.Info("userMiddleware",
		//	zap.String("session", session),
		//)
		if session != "" {

			//id, ok := getInfoFromSession(session)
			//if ok {
			//ctx := context.WithValue(r.Context(), "Info", info)
			//next.ServeHTTP(w, r)

			user, ok := datastore.AdvUserGetBy("token", session)
			if ok {
				ctx.Put("user", user)
				next.ServeHTTP(w, r)
			} else {
				makeError(w, AccessDenied)
			}
			//} // else {
			// 	makeError(w, AccessDenied)
			// }
		} else {
			makeError(w, AccessDenied)
		}
	})
}
func authMiddleware(ctx *stack.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//fmt.Println(ps.ByName("name"), "visit site!")
		// session, err := r.Cookie("session")
		// if err == nil {

		// 	_, ok := s.getInfoFromSession(session.Value)
		user, ok := ctx.Get("user").(datastore.AdvUser)
		if ok && !user.IsEmpty() {
			//ctx := context.WithValue(r.Context(), "Info", info)
			//next.ServeHTTP(w, r)

			next.ServeHTTP(w, r)
		} else {
			makeError(w, AccessDenied)
		}
		// } else {
		// 	makeError(w, AccessDenied)
		// }
	})
}

func corsMiddleware(ctx *stack.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin")) // u.Scheme+"://"+u.Host
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, x-api-token")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		next.ServeHTTP(w, r)
	})
}
