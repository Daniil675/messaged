package server

import (
	"log"
	"net/http"

	"github.com/alexedwards/stack"
	"github.com/julienschmidt/httprouter"
)

func (s Server) Start(port string) {
	//go qiwiInit() //TODO: delete
	router := httprouter.New()

	cors := stack.New(corsMiddleware)
	user := cors.Append(userMiddleware)
	auth := user.Append(authMiddleware)

	//index
	router.GET("/", indexHandler)

	//payments
	//http.HandleFunc("/webhook/ym", paymentYandexHandler)
	//http.HandleFunc("/webhook/qiwi", paymentQiwiHandler)
	router.GET("/webhook/ym", paymentYandexHandler)
	router.GET("/webhook/qiwi", paymentQiwiHandler)

	//bot
	router.POST("/v1/user", userAddHandler)
	router.POST("/v1/users", usersAddHandler)
	router.DELETE("/v1/user", userDeleteHandler)

	//advertiser client handlers
	router.GET("/c/users", Wrap(cors.Then(cUsersGetHandler)))
	router.OPTIONS("/c/users", optionsHandler)

	router.POST("/c/preview", Wrap(auth.Then(cPreviewHandler)))
	router.OPTIONS("/c/preview", optionsHandler)

	router.POST("/c/ad", Wrap(auth.Then(cAdHandler)))
	router.GET("/c/ad/:id", InjectParams(auth.Then(cAdGetHandler)))
	router.OPTIONS("/c/ad", optionsHandler)
	router.OPTIONS("/c/ad/:id", optionsHandler)

	if err := http.ListenAndServe(":"+port, router); err != nil {
		log.Fatalln("Error at ListenAndServe: ", err)
	} else {
		log.Printf("Server stared at localhost:%s\n", port)
	}
}

func InjectParams(hc stack.HandlerChain) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		newHandlerChain := stack.Inject(hc, "params", ps)
		newHandlerChain.ServeHTTP(w, r)
	}
}

func Wrap(hc stack.HandlerChain) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		// newHandlerChain := stack.Inject(hc, "params", ps)
		hc.ServeHTTP(w, r)
	}
}
