package server

type ErrorCode int

const (
	SomethingWentWrong = iota
	KeyIsWrong
	IDIsEmpty
	IDIsWrong
	AccessDenied
	PreviewNotSend
)

//var AllowableFileExtensions = []string{"png", "jpeg", "jpg", "gif", "bmp", "doc", "rtf", "pdf", "txt"}

var ErrorMsgs = map[ErrorCode]string{
	SomethingWentWrong: "Что-то пошло не так.",
	KeyIsWrong:         "Неверный api ключ",
	IDIsEmpty:          "Пустой id",
	IDIsWrong:          "Неверный id",
}
