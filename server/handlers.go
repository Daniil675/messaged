package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"../storage/datastore"
	"../telegram"
	"../utility"
	"github.com/alexedwards/stack"
	"github.com/julienschmidt/httprouter"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

func indexHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "API works")
}

//event
//func (s Server) eventV2AddHandler(w http.ResponseWriter, r *http.Request) {
//	switch r.Method {
//	case "POST":
//		if r.Body != nil {
//			defer r.Body.Close()
//
//			var data datastore.Event
//			err := json.NewDecoder(r.Body).Decode(&data)
//			if err != nil {
//				utility.Logger.Error("eventAddHandler when execute Decode (server).",
//					zap.Error(err),
//					zap.Any("data", data),
//				)
//			}
//
//			key := r.Header.Get("x-api-key")
//
//			if key == "" {
//				makeError(w, KeyIsWrong)
//				return
//			}
//			id, ok := getIDfromToken(key)
//			if !ok {
//				makeError(w, KeyIsWrong)
//				return
//			}
//			if data.UserID == "" {
//				makeError(w, IDIsEmpty)
//				return
//			}
//			if data.Name == "" {
//				makeError(w, NameIsEmpty)
//				return
//			}
//
//			go s.addEvent(id, data)
//		}
//	case "GET":
//		start := time.Date(2018, 5, 20, 0, 0, 0, 0, time.Local)
//		end := time.Date(2018, 5, 20, 11, 6, 5, 0, time.Local)
//		fmt.Println(start, end)
//		responseJSON(w, s.Datastore.EventsGet(start, end))
//	}
//
//}

//payment
func paymentYandexHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	logrus.Info("recieve payment")
	if r.Body != nil {
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		utility.Check("paymentYandexHandler", err)
		logrus.Info(string(body))
		m, _ := url.ParseQuery(string(body))
		var amount decimal.Decimal
		if len(m["amount"]) == 1 {
			logrus.Info(m["amount"][0])
			//amount = decimal.RequireFromString(m["amount"][0])
			amount = decimal.RequireFromString(m["withdraw_amount"][0])
			logrus.Info(amount)
		}
		if len(m["label"]) == 1 {
			id, err := strconv.Atoi(m["label"][0])
			if err == nil {
				ad, ok := datastore.AdGetBy("id", id)
				if ok && amount.Equal(ad.Amount) {
					telegram.AddMsgMI(telegram.AdminChat, fmt.Sprintf("<b>Оплата рекламы.</b>\n"+
						"Сумма: %s\n"+
						"ID рекламы: %d", telegram.MoneyAC.FormatMoneyDecimal(amount), id), telegram.HideBtn)
					//telegram.AddBalanceWithNotification(payer, count)
					var method datastore.MethodT
					switch m["notification_type"][0] {
					case "p2p-incoming":
						method = datastore.YM
					case "card-incoming":
						method = datastore.Card
					default:
						method = datastore.YM
					}
					datastore.PaymentAdd(datastore.Payment{
						AdID:    id,
						Amount:  amount,
						Method:  method,
						Created: time.Now(),
					})
					telegram.ModerateAd(ad)
				} else {
					telegram.AddMsgI(telegram.AdminChat, fmt.Sprintf("Не удалось оплатить рекламу\n"+
						"Сумма: %s\n"+
						"ID рекламы: %d\n"+
						"Стоимость рекламы: %s", amount.String(), id, ad.Amount.String()))
				}
			}
		}
	}
}

func paymentQiwiHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if r.Body != nil {
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		utility.Check("paymentQiwiHandler", err)
		logrus.Info(string(body))
		var data QiwiPayment
		err = json.Unmarshal(body, &data)

		if err == nil {
			id, err := strconv.Atoi(data.Payment.Comment)
			if err == nil {
				ad, ok := datastore.AdGetBy("id", id)
				if ok && data.Payment.Sum.Amount.Equal(ad.Amount) {
					telegram.AddMsgMI(telegram.AdminChat, fmt.Sprintf("<b>Оплата рекламы.</b>\n"+
						"Сумма: %s\n"+
						"ID рекламы: %d", telegram.MoneyAC.FormatMoneyDecimal(data.Payment.Sum.Amount), id), telegram.HideBtn)
					datastore.PaymentAdd(datastore.Payment{
						AdID:    id,
						Amount:  data.Payment.Sum.Amount,
						Method:  datastore.Qiwi,
						Created: time.Now(),
					})
					telegram.ModerateAd(ad)
				} else {
					telegram.AddMsgI(telegram.AdminChat, fmt.Sprintf("<b>Не удалось оплатить рекламу</b>\n"+
						"Сумма: %s\n"+
						"ID рекламы: %d\n"+
						"Стоимость рекламы: %s", data.Payment.Sum.Amount.String(), id, ad.Amount.String()))
				}
			}
		} else {
			logrus.Error(err)
		}
	}
}

func userAddHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	values := r.URL.Query()

	key := values.Get("key")
	if key == "" {
		makeError(w, KeyIsWrong)
		return
	}
	botID, ok := getIDfromToken(key)
	if !ok {
		makeError(w, KeyIsWrong)
		return
	}
	userIDStr := values.Get("id")
	if userIDStr == "" {
		makeError(w, IDIsEmpty)
		return
	}
	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		makeError(w, IDIsWrong)
		return
	}
	datastore.UserToBotAdd(userID, botID)
}

func usersAddHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	values := r.URL.Query()

	key := values.Get("key")
	if key == "" {
		makeError(w, KeyIsWrong)
		return
	}
	botID, ok := getIDfromToken(key)
	if !ok {
		makeError(w, KeyIsWrong)
		return
	}
	usersIDStr := values.Get("ids")
	if usersIDStr == "" {
		makeError(w, IDIsEmpty)
		return
	}

	for _, id := range strings.Split(usersIDStr, ",") {
		userID, err := strconv.Atoi(id)
		if err != nil {
			makeError(w, IDIsWrong)
			return
		}
		datastore.UserToBotAdd(userID, botID)
	}
}
func userDeleteHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	values := r.URL.Query()

	key := values.Get("key")
	if key == "" {
		makeError(w, KeyIsWrong)
		return
	}
	botID, ok := getIDfromToken(key)
	if !ok {
		makeError(w, KeyIsWrong)
		return
	}
	userIDStr := values.Get("id")
	if userIDStr == "" {
		makeError(w, IDIsEmpty)
		return
	}
	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		makeError(w, IDIsWrong)
		return
	}
	if ok := datastore.BotUserDeactivate(userID, botID); ok {
		makeError(w, SomethingWentWrong)
	}
}
func cUsersGetHandler(ctx *stack.Context, w http.ResponseWriter, r *http.Request) {
	if activeCount, ok := usersCount(); ok {
		responseJSON(w, UsersGetRes{Count: activeCount + 90})
	} else {
		makeError(w, SomethingWentWrong)
	}
}

func cPreviewHandler(ctx *stack.Context, w http.ResponseWriter, r *http.Request) {
	if user, ok := check(ctx, w); ok {
		if r.Body == nil {
			makeError(w, SomethingWentWrong)
		}
		var data datastore.AdT
		err := json.NewDecoder(r.Body).Decode(&data)
		utility.Check("cPreviewHandler", err)

		if ok = telegram.SendAdvMsg(user.ID, formatString(data.Text)); !ok {
			makeError(w, PreviewNotSend)
		}
	} else {
		makeError(w, AccessDenied)
	}
}

func cAdHandler(ctx *stack.Context, w http.ResponseWriter, r *http.Request) {
	if user, ok := check(ctx, w); ok {
		if r.Body == nil {
			makeError(w, SomethingWentWrong)
		}
		defer r.Body.Close()

		var data datastore.AdT
		err := json.NewDecoder(r.Body).Decode(&data)
		utility.Check("cPreviewHandler", err)

		logrus.Info(data)
		data.OwnerID, data.Stage, data.Created = user.ID, datastore.Created, time.Now()
		//data.Buttons = append(data.Buttons, datastore.Button{})
		switch data.T {
		case datastore.DAll:
			activeCount, ok := usersCount()
			if ok {
				data.Amount = decimal.New(int64(activeCount), 0).Mul(priceForMsgAll)
			} else {
				makeError(w, SomethingWentWrong)
			}
		case datastore.DCount:
			if data.Count > 0 {
				data.Amount = decimal.New(int64(data.Count), 0).Mul(priceForMsg)
			} else {
				makeError(w, SomethingWentWrong)
			}
		default:
			makeError(w, SomethingWentWrong)
			return
		}
		data.ID, ok = datastore.AdAdd(data)
		if ok {
			responseJSON(w, data)
		} else {
			return
		}
		telegram.SendAdvMsg(user.ID, formatString(data.Text))
		telegram.PayMsg(user.ID, data.ID, data.Amount)
	}
}

func cAdGetHandler(ctx *stack.Context, w http.ResponseWriter, r *http.Request) {
	if user, ok := check(ctx, w); ok {
		if params, ok := ctx.Get("params").(httprouter.Params); ok {
			id, _ := strconv.Atoi(params.ByName("id"))
			if !ok && id > 0 {
				makeError(w, SomethingWentWrong)
				return
			}
			ad, ok := datastore.AdGetBy("id", id)
			if !ok {
				makeError(w, SomethingWentWrong)
				return
			}
			if ad.OwnerID == user.ID {
				responseJSON(w, ad)
			} else {
				makeError(w, AccessDenied)
			}
		} else {
			makeError(w, SomethingWentWrong)
		}
	}
}
func optionsHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin")) // u.Scheme+"://"+u.Host
	w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, x-api-token")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
}
