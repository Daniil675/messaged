package server

import (
	"../storage/datastore"
	"../utility"
	"encoding/json"
	"github.com/alexedwards/stack"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strconv"
)

const (
	round = 10
)

var (
	priceForMsg    = decimal.RequireFromString("0.05")
	priceForMsgAll = decimal.RequireFromString("0.045")
)

func responseJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func makeError(w http.ResponseWriter, errCode ErrorCode) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	e := Err{Code: errCode}
	responseJSON(w, e)
}
func getIDfromToken(key string) (int, bool) {
	bot, ok := datastore.BotGetBy("api_key", key)
	return bot.ID, ok
}
func check(ctx *stack.Context, w http.ResponseWriter) (datastore.AdvUser, bool) {
	user, ok := ctx.Get("user").(datastore.AdvUser)
	if !ok || user.IsEmpty() {
		makeError(w, SomethingWentWrong)
		return datastore.AdvUser{}, ok
	}
	return user, ok
}

func qiwiInit() {
	req, err := http.NewRequest("PUT", "https://edge.qiwi.com/payment-notifier/v1/hooks", nil)
	q := req.URL.Query()
	q.Add("hookType", strconv.Itoa(1))
	q.Add("param", "http://api.messagead.co/webhook/qiwi")
	q.Add("txnType", strconv.Itoa(0))

	req.URL.RawQuery = q.Encode()
	req.Header.Set("Authorization", "Bearer ")
	resp, err := http.DefaultClient.Do(req)
	utility.Check("qiwiInit", err)

	if resp != nil {
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		utility.Check("qiwiInit", err)
		logrus.Info(string(body))
	}
}
func formatString(str string) string {
	//str = strings.Replace(str, "<p>", "", -1)
	//str = strings.Replace(str, "</p>", "", -1)
	//str = strings.Replace(str, "<br>", "\n", -1)
	//str = strings.Replace(str, " target=\"_blank\">", ">", -1)
	//str = strings.Replace(str, "<pre class=\"ql-syntax\" spellcheck=\"false\">", "<pre>", -1)
	//str = str + "\n\n" +
	//	"Реклама через @MessageAd"
	logrus.Info(str)
	return str
}

func usersCount() (int, bool) {
	activeCount, ok := datastore.TelegramUsersActiveCount()
	if ok {
		activeCount++
		activeCount += round - (activeCount % round)
		return activeCount, ok
	} else {
		return 0, false
	}
}
