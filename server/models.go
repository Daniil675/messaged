package server

import (
	"github.com/shopspring/decimal"
	"time"
)

type (
	Server struct {
		//Sessions  *session.SessionStorage
		//API       *api.API
		//TG        *telegram.Bot
		//Bot       *vk.Bot
	}
	Auth struct {
		Token string `json:"token"`
	}

	Err struct {
		Code ErrorCode `json:"error_code"`
	}
	QiwiPayment struct {
		MessageID string `json:"messageId"`
		HookID    string `json:"hookId"`
		Payment   struct {
			TxnID     string    `json:"txnId"`
			Date      time.Time `json:"date"`
			Type      string    `json:"type"`
			Status    string    `json:"status"`
			ErrorCode string    `json:"errorCode"`
			PersonID  int64     `json:"personId"`
			Account   string    `json:"account"`
			Comment   string    `json:"comment"`
			Provider  int       `json:"provider"`
			Sum       struct {
				Amount   decimal.Decimal `json:"amount"`
				Currency int             `json:"currency"`
			} `json:"sum"`
			Commission struct {
				Amount   decimal.Decimal `json:"amount"`
				Currency int             `json:"currency"`
			} `json:"commission"`
			Total struct {
				Amount   decimal.Decimal `json:"amount"`
				Currency int             `json:"currency"`
			} `json:"total"`
			SignFields string `json:"signFields"`
		} `json:"payment"`
		Hash    string `json:"hash"`
		Version string `json:"version"`
		Test    bool   `json:"test"`
	}
	Img struct {
		Name string `json:"name"`
	}
	UsersGetRes struct {
		Count int `json:"count"`
	}
)
