package telegram

import (
	"../storage/datastore"
	"errors"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"go.uber.org/ratelimit"
	"sync"
	"sync/atomic"
	"time"
)

const (
	ErrBadBot = "bad bot"
)

var (
	msgCountPerSecond = 20
	//blockSize         = 50
	paginate      = 50
	payToBotOwner = decimal.RequireFromString("0.04")
)

type (
	AdBot struct {
		ID  int
		Api *tgbotapi.BotAPI
		//pay decimal.Decimal
		users *int64
		c     chan bool
	}
	BotStore struct {
		AdBots map[int]*AdBot
		mu     *sync.RWMutex
	}
)

func (bot *AdBot) start() {
	rl := ratelimit.New(msgCountPerSecond)
	for {
		rl.Take()
		bot.c <- true
	}
}
func (bot *AdBot) send(c tgbotapi.Chattable) (tgbotapi.Message, error) {
	<-bot.c
	return bot.Api.Send(c)
}
func newAdBot(id int) (*AdBot, bool) {
	b, _ := datastore.BotGetBy("id", id)
	if bot, ok := NewBot(b.Token); ok {
		var c int64
		adBot := AdBot{
			ID:  b.ID,
			Api: bot,
			//pay: decimal.Zero,
			users: &c,
			c:     make(chan bool),
		}
		go adBot.start()
		return &adBot, true
	}
	return &AdBot{}, false
}
func newBotStore() *BotStore {
	return &BotStore{
		AdBots: map[int]*AdBot{},
		mu:     &sync.RWMutex{},
	}
}
func (bs *BotStore) send(b, target int, c tgbotapi.Chattable) (tgbotapi.Message, error) {
	var (
		bot *AdBot
		ok  bool
	)
	if bot, ok = bs.AdBots[b]; !ok {
		if bot, ok = newAdBot(b); ok {
			bs.mu.Lock()
			bs.AdBots[b] = bot
			bs.mu.Unlock()
			//send
			//bs.mu.RLock()
			//defer bs.mu.RUnlock()
			//msg, err := bot.send(c)
			//if err == nil {
			//	bot.pay.Add(payToBotOwner)
			//} else if err.Error() == "Forbidden: bot was blocked by the user" {
			//	go datastore.BotUserDeactivate(target, b)
			//}
			//go func() {
			//	datastore.TelegramUserEditOne("id", target, "last_msg", time.Now())
			//	datastore.BotUserEditOne("user_id", target, "last_msg", time.Now())
			//}()
			//return msg, err
		} else {
			return tgbotapi.Message{}, errors.New(ErrBadBot)
		}
	}
	bs.mu.RLock()
	defer bs.mu.RUnlock()
	msg, err := bot.send(c)
	if err != nil {
		logrus.Error(err)
	}
	if target == 145771050 {
		logrus.Info("send", msg)
	}
	if err == nil {
		atomic.AddInt64(bot.users, 1)
	} else if err.Error() == "Forbidden: bot was blocked by the user" {
		datastore.BotUserDeactivate(target, b)
	} else if err.Error() == "Forbidden: user is deactivated" {
		datastore.TelegramUserEditOne("id", target, "is_active", false)
		datastore.BotUserEditOne("user_id", target, "is_active", false)
	} else {
		datastore.BotUserDeactivate(target, b)
	}
	datastore.BotUserEditLastMsg(target, bot.ID)
	return msg, err
}

func (bs *BotStore) payout(t datastore.DistributionT) {
	for _, b := range bs.AdBots {
		bot, _ := datastore.BotGetBy("id", b.ID)
		pay := payToBotOwner.Mul(decimal.New(*b.users, 0))
		datastore.TransactionAdd(datastore.Transaction{
			BotID:   bot.ID,
			Amount:  pay,
			T:       t,
			Users:   int(*b.users),
			Created: time.Now(),
		})
		AddBalanceWithNotification(bot, pay)
	}
}

func AdSender() {
	for {
		ads, _ := datastore.AdsGetsDCountByCreated(1)
		if len(ads) == 1 {
			ad := ads[0]
			datastore.AdEditOne("id", ad.ID, "stage", datastore.DStart)
			adStartMsg(ad.ID, ad.OwnerID)
			ad.Text = formatAdMsg(ad.Text)
			//switch ad.T {
			//case datastore.DAll:
			//	distributionToAll(ad)
			//case datastore.DCount:
			distribution(ad)
			//case datastore.DBot:
			//
			//}
			datastore.AdEditOne("id", ad.ID, "stage", datastore.DFinish)
			adSuccessMsg(ad.ID, ad.OwnerID)
		}
	}
}

func distribution(ad datastore.AdT) {
	count := int64(ad.Count)
	invoice := decimal.New(count, 0).Mul(payToBotOwner)
	bs := newBotStore()

	msg := tgbotapi.NewMessage(0, ad.Text)
	msg.ParseMode = tgbotapi.ModeHTML

	var wg sync.WaitGroup

	for count > 0 {
		var users []datastore.TelegramUser
		if int(count) >= paginate {
			users, _ = datastore.TelegramUsersGetsByLastMsg(paginate)
		} else {
			users, _ = datastore.TelegramUsersGetsByLastMsg(int(count))
		}

		for _, user := range users {
			wg.Add(1)
			go func(id int) {
				distributionMsg(&wg, bs, msg, id, &count, -1)
				wg.Done()
			}(user.ID)
			//logrus.WithField("id", user.ID).Info("Send to")
		}
		wg.Wait()
		logrus.Info("test cycle")
	}
	datastore.AdEditOne("id", ad.ID, "invoice", invoice)
	bs.payout(datastore.DCount)
	adTotalMsg(ad, invoice)
}
func distributionToAll(ad datastore.AdT) {
	run := true

	msg := tgbotapi.NewMessage(0, ad.Text)
	msg.ParseMode = tgbotapi.ModeHTML

	bs := newBotStore()

	var (
		users   []datastore.TelegramUser
		lastID  int
		wg      sync.WaitGroup
		count   int64
		isFirst bool
	)

	for run {
		if isFirst {
			users = datastore.TelegramUsersGetsByCursor(true, 0, paginate)
			isFirst = false
		} else {
			users = datastore.TelegramUsersGetsByCursor(false, lastID, paginate)
		}
		//logrus.Info(users)
		if len(users) > 0 {
			for _, user := range users {
				wg.Add(1)
				go func(id int) {
					distributionMsg(&wg, bs, msg, id, &count, 1)
					wg.Done()
				}(user.ID)
			}
			lastID = users[len(users)-1].ID
			wg.Wait()
		} else {
			run = false
		}
		logrus.Info("test cycle")
	}
	invoice := decimal.New(count, 0).Mul(payToBotOwner)
	datastore.AdEditOne("id", ad.ID, "invoice", invoice)
	bs.payout(datastore.DAll)
	adTotalMsg(ad, invoice)
}
func distributionMsg(wg *sync.WaitGroup, bs *BotStore, msg tgbotapi.MessageConfig, userID int, count *int64, delta int64) {
	msg.ChatID = int64(userID)
	bots, ok := datastore.BotUsersGetsByLastAd(userID, 1)
	if ok {
		if len(bots) >= 1 {
			if _, err := bs.send(bots[0].BotID, userID, msg); err == nil {
				atomic.AddInt64(count, delta)
				if delta < 0 {
					datastore.TelegramUserEditOne("id", userID, "last_msg", time.Now())
				}
			} else {
				distributionMsg(wg, bs, msg, userID, count, delta)
			}
		} else {
			datastore.TelegramUserEditOne("id", userID, "is_active", false)
		}
	} else {
		datastore.TelegramUserEditOne("id", userID, "is_active", false)
	}
}
