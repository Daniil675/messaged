package telegram

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

type (
	BotT struct {
		*tgbotapi.BotAPI
	}
	Msg struct {
		Text   string
		ChatID int64
		Markup interface{}
	}
	PayloadQiwi struct {
		ID            string        `json:"id"`
		Sum           Sum           `json:"sum"`
		PaymentMethod PaymentMethod `json:"paymentMethod"`
		Comment       string        `json:"comment"`
		Fields        Fields        `json:"fields"`
	}
	Sum struct {
		Amount   float64 `json:"amount"`
		Currency string  `json:"currency"`
	}
	PaymentMethod struct {
		Type      string `json:"type"`
		AccountID string `json:"accountId"`
	}
	Fields struct {
		Account string `json:"account"`
	}
	YMPayment struct {
		Status                  string  `json:"status"`
		Error                   string  `json:"error"`
		Balance                 float64 `json:"balance"`
		RequestID               string  `json:"request_id"`
		RecipientIdentified     bool    `json:"recipient_identified"`
		MultipleRecipientsFound bool    `json:"multiple_recipients_found"`
		RecipientAccountType    string  `json:"recipient_account_type"`
		RecipientAccountStatus  string  `json:"recipient_account_status"`
		ContractAmount          float64 `json:"contract_amount"`
		Fees                    struct {
			Service float64 `json:"service"`
		} `json:"fees"`
		RecipientMaskedName    interface{} `json:"recipient_masked_name"`
		RecipientMaskedAccount string      `json:"recipient_masked_account"`
		MoneySource            struct {
			Wallet struct {
				Allowed bool `json:"allowed"`
			} `json:"wallet"`
		} `json:"money_source"`
	}
	QiwiAnswer struct {
		ID     string `json:"id"`
		Terms  string `json:"terms"`
		Fields struct {
			Account string `json:"account"`
		} `json:"fields"`
		Sum struct {
			Amount   int    `json:"amount"`
			Currency string `json:"currency"`
		} `json:"sum"`
		Transaction struct {
			ID    string `json:"id"`
			State struct {
				Code string `json:"code"`
			} `json:"state"`
		} `json:"transaction"`
		Comment string `json:"comment"`
		Source  string `json:"source"`
	}
)
