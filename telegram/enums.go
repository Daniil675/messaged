package telegram

type CallbackType int

const (
	сBot = iota
	cApiKey
	cBotApiKeyRevoke
	сBotStats
	cBotRemove
	cHide
	cAdApprove
	cAdBlock
)
