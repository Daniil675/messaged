package telegram

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	"math/rand"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/leekchan/accounting"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"go.uber.org/ratelimit"

	"../storage/datastore"
	"../utility"
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

var (
	Bot         *BotT
	AdvBot      *BotT
	location, _ = time.LoadLocation("Europe/Moscow")
	//keyboard    = tgbotapi.NewReplyKeyboard(
	//	tgbotapi.NewKeyboardButtonRow(
	//		tgbotapi.NewKeyboardButton("Лечь спать прямо сейчас😴"),
	//	),
	//)
	adminID     = 145771050
	adminID64   = int64(adminID)
	AdminChat   = -355047587
	adminChat64 = int64(AdminChat)
	//statMsgID   = 0
	minimumWithdraw     = decimal.RequireFromString("100")
	emptyInlineKeyboard = mKeyboard()
	canSend             = make(chan bool)
	MoneyAC             = accounting.NewAccounting("руб.", 2, " ", ",", "%v %s", "-%v %s", "0 %s")
	countAC             = accounting.NewAccounting("", 0, " ", "", "%v", "-%v", "0")
	ac                  = countAC.FormatMoneyInt
	//aac3                = accounting.Accounting{
	//	Symbol:         "₽",
	//	Precision:      2,
	//	Thousand:       " ",
	//	Decimal:        ",",
	//	Format:         "%v %s",
	//	FormatNegative: "(%v) %s",
	//}
)

func init() {
	Bot = new(BotT)
}

func New(token string) {
	bot, _ := NewBot(token)
	Bot = &BotT{BotAPI: bot}
}
func NewBot(token string) (*tgbotapi.BotAPI, bool) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		logrus.WithField("token", token).Error("NewBot", err)
		return nil, false
	}
	return bot, true
}
func Start(wg *sync.WaitGroup) {
	go senderStart()
	go AdSender()
	go StartAdv()
	go statsUpdater()
	//collectAndSendAnalytics()
	defer wg.Done()
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := Bot.GetUpdatesChan(u)
	utility.Check("Error in Start when execute bot.GetUpdatesChan (telegram).", err)

	for update := range updates {
		go handleUpdate(update)
	}
}
func StartAdv() {
	bot, _ := NewBot("817027092:AAHpnH45Z8KG4RTfDGoVbAJpW371lZNAbYg")
	AdvBot = &BotT{BotAPI: bot}
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, _ := AdvBot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}
		logrus.Info(update.Message)
		from := update.Message.From.ID
		chat := update.Message.Chat.ID
		if chat == int64(from) {
			user := checkAdvUser(from, update.Message.From.UserName)
			//arg := update.Message.CommandArguments()
			switch update.Message.Command() {
			case "start":
				SendAdvMsg(from, "Здравствуйте! Этот бот поможет рекламодателям заказать эффективную рекламу. <a href=\"https://telegra.ph/MessageAdv-08-12\">Инструкция</a> (или https://tgraph.io/MessageAdv-08-12)")
				webMsg(chat, user.Token)
			case "help":
				SendAdvMsg(from, "/help - просмотреть это сообщение\n"+
					"/web - доступ к сайту для заказа рекламы\n"+
					"/balance - баланс и ваш id")
			case "web":
				token := uuid.Must(uuid.NewV4())
				datastore.AdvUserEditOne("id", user.ID, "token", token)
				webMsg(chat, token)
			case "balance":
				AddMsgI(from, fmt.Sprintf("Ваш id: %d\n"+
					"Ваш баланс: <b>%s</b>", user.ID, MoneyAC.FormatMoneyDecimal(user.Balance)))
			default:
			}
		}
	}
}
func handleUpdate(update tgbotapi.Update) {
	if update.CallbackQuery != nil {
		logrus.Info(update.CallbackQuery)
		chat := update.CallbackQuery.Message.Chat.ID
		from := int(chat)
		//bot.AnswerCallbackQuery(tgbotapi.NewCallback(update.CallbackQuery.ID,))
		data := update.CallbackQuery.Data
		index := strings.Index(data, " ")
		ctype, _ := strconv.Atoi(data[:index])
		data = data[index+1:]
		if chat == adminChat64 {
			switch ctype {
			case cHide:
				send(tgbotapi.NewDeleteMessage(chat, update.CallbackQuery.Message.MessageID))
			case cAdApprove:
				index := strings.Index(data, " ")
				if ad, ok := GetAdFromCallback(data, index); ok {
					mid, _ := strconv.Atoi(data[index+1:])
					send(tgbotapi.NewDeleteMessage(chat, mid))
					send(tgbotapi.NewDeleteMessage(chat, update.CallbackQuery.Message.MessageID))
					datastore.AdEditOne("id", ad.ID, "stage", datastore.Approved)
					SendAdvMsg(ad.OwnerID, fmt.Sprintf("Реклама #%d успешно прошла модерацию. Ожидаем начало рассылки.", ad.ID))
					switch ad.T {
					case datastore.DAll:
						go func() {
							datastore.AdEditOne("id", ad.ID, "stage", datastore.DStart)
							adStartMsg(ad.ID, ad.OwnerID)
							ad.Text = formatAdMsg(ad.Text)
							distributionToAll(ad)
							datastore.AdEditOne("id", ad.ID, "stage", datastore.DFinish)
							adSuccessMsg(ad.ID, ad.OwnerID)
						}()
					case datastore.DBot:

					}
				}
			case cAdBlock:
				index := strings.Index(data, " ")
				if ad, ok := GetAdFromCallback(data, index); ok {
					mid, _ := strconv.Atoi(data[index+1:])
					send(tgbotapi.NewDeleteMessage(chat, mid))
					send(tgbotapi.NewDeleteMessage(chat, update.CallbackQuery.Message.MessageID))
					datastore.AdEditOne("id", ad.ID, "stage", datastore.Blocked)
					SendAdvMsg(ad.OwnerID, fmt.Sprintf("Реклама #%d не прошла модерацию. Ваш баланс пополнен на стоимость рекламы. Для того чтобы оплатить этими деньгами новую рекламу или вернуть деньги обратитесь к @MessageAdvSupport", ad.ID))
					datastore.AddAdvBalance(ad.OwnerID, ad.Amount)
				}
			}
		} else {
			index = strings.Index(data, " ")
			if bot, ok := GetBotFromCallback(data, index); ok {
				if bot.OwnerID == from {
					comebackButton := mKeyboardRow(makeIB("🔙 Вернуться к боту", strconv.Itoa(bot.ID), сBot))
					switch ctype {
					case сBot:
						m := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, fmt.Sprintf("%s (@%s)\nЧто вы хотите сделать?", bot.Name, bot.Username))
						m.ParseMode = tgbotapi.ModeHTML
						Bot.Send(m)

						apiKeyBtn := mKeyboardRow(makeIB("API Ключ", strconv.Itoa(bot.ID), cApiKey))
						statsBtn := mKeyboardRow(makeIB("Статистика", strconv.Itoa(bot.ID), сBotStats))
						removeBtn := mKeyboardRow(makeIB("Удалить бота", fmt.Sprintf("%d %d", bot.ID, 0), cBotRemove))
						keyboard := mKeyboard(apiKeyBtn, statsBtn, removeBtn)
						rm := tgbotapi.NewEditMessageReplyMarkup(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, keyboard)
						Bot.Send(rm)
					case cApiKey:
						m := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, fmt.Sprintf("Это api ключ для %s (@%s):\n\n<code>%s</code>", bot.Name, bot.Username, bot.ApiKey))
						m.ParseMode = tgbotapi.ModeHTML
						Bot.Send(m)

						revokeButton := mKeyboardRow(makeIB("Сгенерировать новый api ключ", strconv.Itoa(bot.ID), cBotApiKeyRevoke))
						keyboard := mKeyboard(revokeButton, comebackButton)
						rm := tgbotapi.NewEditMessageReplyMarkup(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, keyboard)
						Bot.Send(rm)
					case cBotApiKeyRevoke:
						key := utility.GenerateApiKey()
						datastore.BotEditOne("id", bot.ID, "api_key", key)
						m := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, fmt.Sprintf("Api ключ для %s (@%s) был сгенерирован заново. Новый api ключ:\n\n<code>%s</code>", bot.Name, bot.Username, key))
						m.ParseMode = tgbotapi.ModeHTML
						Bot.Send(m)

						keyboard := mKeyboard(comebackButton)
						rm := tgbotapi.NewEditMessageReplyMarkup(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, keyboard)
						Bot.Send(rm)
					case сBotStats:
						/*m := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, fmt.Sprintf("Статистика для бота %s (@%s).\n\n"+
						"Количество пользователей: 777\n"+
						"Средний заработок с одного пользователя: 1.7 р.\n"+
						"Уникальность аудитории: 98.7%%\n"+
						"Среднестатистический шанс того что вы выиграете пользователя: 99.9%%", bot.Name, bot.Username))
						*/
						usersCount, _ := datastore.BotUsersCount(bot.ID)
						usersActiveCount, _ := datastore.BotUsersActiveCount(bot.ID)
						m := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, fmt.Sprintf("Статистика для бота %s (@%s).\n\n"+
							"Количество пользователей: %s\n"+
							"Из них активных: %s\n", bot.Name, bot.Username, ac(usersCount), ac(usersActiveCount)))
						m.ParseMode = tgbotapi.ModeHTML
						Bot.Send(m)

						keyboard := mKeyboard(comebackButton)
						rm := tgbotapi.NewEditMessageReplyMarkup(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, keyboard)
						Bot.Send(rm)
					case cBotRemove:
						rid, _ := strconv.Atoi(data[index+1:]) //rid - remove id(stage)
						switch rid {
						case 0:
							m := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, fmt.Sprintf("Вы собираетесь удалить своего бота <b>%s</b> @%s. Все верно?", bot.Name, bot.Username))
							m.ParseMode = tgbotapi.ModeHTML
							Bot.Send(m)

							cancelFirstBtn := mKeyboardRow(makeIB("Нет, забудь", strconv.Itoa(bot.ID), сBot))
							cancelSecondBtn := mKeyboardRow(makeIB("Нет", strconv.Itoa(bot.ID), сBot))
							deleteBtn := mKeyboardRow(makeIB("Да, удалить бота", fmt.Sprintf("%d %d", bot.ID, 1), cBotRemove))
							buttons := [][]tgbotapi.InlineKeyboardButton{cancelFirstBtn, cancelSecondBtn, deleteBtn}
							rand.Shuffle(len(buttons), func(i, j int) { buttons[i], buttons[j] = buttons[j], buttons[i] })
							buttons = append(buttons, comebackButton)

							keyboard := mKeyboard(buttons...)
							rm := tgbotapi.NewEditMessageReplyMarkup(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, keyboard)
							Bot.Send(rm)
						case 1:
							m := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, fmt.Sprintf("Вы <b>ПОЛНОСТЬЮ</b> уверены, что хотите удалить <b>%s</b> @%s?", bot.Name, bot.Username))
							m.ParseMode = tgbotapi.ModeHTML
							Bot.Send(m)

							cancelFirstBtn := mKeyboardRow(makeIB("Конечно нет!", strconv.Itoa(bot.ID), сBot))
							cancelSecondBtn := mKeyboardRow(makeIB("Нет!", strconv.Itoa(bot.ID), сBot))
							deleteBtn := mKeyboardRow(makeIB("Да, я уверен на 100%", fmt.Sprintf("%d %d", bot.ID, 2), cBotRemove))
							buttons := [][]tgbotapi.InlineKeyboardButton{cancelFirstBtn, cancelSecondBtn, deleteBtn}
							rand.Shuffle(len(buttons), func(i, j int) { buttons[i], buttons[j] = buttons[j], buttons[i] })
							buttons = append(buttons, comebackButton)

							keyboard := mKeyboard(buttons...)
							rm := tgbotapi.NewEditMessageReplyMarkup(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, keyboard)
							Bot.Send(rm)
						case 2:
							datastore.BotDelete(bot.ID)
							m := tgbotapi.NewEditMessageText(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, fmt.Sprintf("Вы удалили <b>%s</b> @%s.", bot.Name, bot.Username))
							m.ParseMode = tgbotapi.ModeHTML
							Bot.Send(m)

							rm := tgbotapi.NewEditMessageReplyMarkup(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Message.MessageID, emptyInlineKeyboard)
							Bot.Send(rm)
						}
					}
				} else {
					AddMsgI(from, "Вы не имеете доступа к этому боту!")
				}
			} else {
				AddMsgI(from, "Такого бота не существует!")
			}
		}
	}
	if update.Message != nil {
		logrus.Info(update.Message)
		from := update.Message.From.ID
		chat := update.Message.Chat.ID
		if chat == int64(from) {
			user := checkUser(from, update.Message.From.UserName)
			arg := update.Message.CommandArguments()
			switch update.Message.Command() {
			case "start":
				AddMsgI(from, "Здравствуйте! Этот бот поможет владельцам ботов заработатать на рекламе в своих ботах. <a href=\"https://telegra.ph/MessageAd-08-12\">Инструкция</a> (или https://tgraph.io/MessageAd-08-12)")
				checkUser(from, update.Message.From.UserName)
			case "help":
				AddMsgI(from, "/help - просмотреть это сообщение\n"+
					"/mybots - список ваших ботов\n"+
					"/newbot *токен* - добавить нового бота или обновить токен к старому\n"+
					"/api - документация по api\n"+
					"/balance - узнать свой баланс\n"+
					"/wym *номер яндекс кошелька* - вывести средства на Яндекс.Деньги\n"+
					"/wqiwi *номер телефона* - вывести средства на qiwi")
			case "newbot":
				if arg != "" {
					bot, ok := NewBot(arg)
					if ok {
						b, ok := datastore.BotGetBy("id", bot.Self.ID)
						if ok {
							if arg == b.Token {
								AddMsgI(from, "Бот уже добавлен.")
							} else {
								AddMsgI(from, "Токен обновлен.")
								if from != b.OwnerID {
									AddMsgI(from, fmt.Sprintf("Теперь вы владелец бота <b>%s</b> (@%s)", bot.Self.FirstName, bot.Self.UserName))
									datastore.BotEditOne("id", bot.Self.ID, "owner_id", from)
								}
							}
						} else {
							key := utility.GenerateApiKey()
							AddMsgI(from, fmt.Sprintf("<b>%s</b> (@%s)\n"+
								"Успешно добавлен!\n\n"+
								"Ключ для доступа к API: <code>%s</code>\n"+
								"Для того чтобы узнать как пользоваться API отправьте /api\n"+
								"Если вы измените телеграмм токен бота то воспользуйтесь командой /newbot снова, данные о пользователях бота сохранятся.", bot.Self.FirstName, bot.Self.UserName, key))
							datastore.BotAdd(datastore.Bot{
								ID:       bot.Self.ID,
								Token:    arg,
								ApiKey:   key,
								Username: bot.Self.UserName,
								Name:     bot.Self.FirstName,
								OwnerID:  from,
							})
							AddMsgMI(AdminChat, fmt.Sprintf("<b>Новый бот</b>\n"+
								"Название: %s\n"+
								"Username: @%s", bot.Self.FirstName, bot.Self.UserName), HideBtn)
						}
					} else {
						AddMsgI(from, "Неправильный токен!")
					}
				} else {
					AddMsgI(from, "Отсутствует токен!")
				}
			case "mybots":
				bots, ok := datastore.BotsGetsBy("owner_id", from)
				if ok {
					if len(bots) == 0 {
						AddMsgI(from, "У вас пока нет ботов!\n"+
							"Вы можете добавить бота используя команду /newbot")
					} else {
						var buttons [][]tgbotapi.InlineKeyboardButton
						for _, bot := range bots {
							buttons = append(buttons, mKeyboardRow(makeIB("@"+bot.Username, strconv.Itoa(bot.ID), сBot)))
						}
						keyboard := mKeyboard(buttons...)
						AddMsgMI(from, "Ваши боты:", keyboard)
					}
				}
			case "balance":
				AddMsgI(from, fmt.Sprintf("Ваш баланс: <b>%s</b>", MoneyAC.FormatMoneyDecimal(user.Balance)))
			case "api":
				AddMsgI(from, "Описание api доступно по ссылке: https://gist.github.com/Daniil675/9cb9a0d3265c5c2939b0ea2e3b9c62c6\n"+
					"Мы рекомендуем присылать нам данные о новом пользователе каждый раз когда он присылает /start в вашем боте. Даже если вы уже присылали данные о пользователе ранее дубликата не будет!")
			//case "wallets":
			//	AddMsgI(from, fmt.Sprintf("Банковская карта: *%s* (Для изменения отправьте \"/wсard Номер карты\")\n"+
			//		"Яндекс Деньги: *%s* (Для изменения отправьте \"/wym Номер кошелька в Яндекс.Деньги\")\n"+
			//		"WebMoney: *%s* (Для изменения отправьте \"/wwm Номер кошелька в WebMoney\")\n"+
			//		"Qiwi: *%s* (Для изменения отправьте \"/wqiwi Номер телефона\")\n", user.Wallets.Card, user.Wallets.YM, user.Wallets.WM, user.Wallets.Qiwi))
			//case "wcard":
			//	user.Wallets.Card = arg
			//	ok := datastore.UserEditOne("id", from, "wallets", user.Wallets)
			//	if ok {
			//		AddMsgI(from, "Номер банковской карты успешно изменен!")
			//	}
			case "wym":
				withdraw(arg, user, datastore.YM)
			case "wqiwi":
				withdraw(arg, user, datastore.Qiwi)
			//case "wwm":
			//	user.Wallets.WM = arg
			//	ok := datastore.UserEditOne("id", from, "wallets", user.Wallets)
			//	if ok {3
			//		AddMsgI(from, "Номер кошелька в WebMoney успешно изменен!")
			//	}
			case "withdraw":
			default:

			}
		} else if chat == adminChat64 {
			if command := update.Message.Command(); command != "" {
				arg := update.Message.CommandArguments()
				switch command {
				//case "stats":
				//	if chat == adminID64 {
				//		AddMsg(chat, analytics.GetAnalyticsPermanent())
				//	}
				case "ad":
					ad, _ := datastore.AdGetBy("id", arg)
					AddMsgI(AdminChat, fmt.Sprintf("<b>Реклама #%d</b>\n"+
						"Стоимость: %s", ad.ID, ad.Amount.String()))
				case "pay":
					args := strings.Split(arg, " ")
					if len(args) == 2 {
						ad, _ := datastore.AdGetBy("id", args[0])
						AddMsgMI(AdminChat, fmt.Sprintf("<b>Оплата рекламы.</b>\n"+
							"ID рекламы: %d\n"+
							"Сумма: %s", ad.ID, MoneyAC.FormatMoneyDecimal(ad.Amount)), HideBtn)
						method, _ := strconv.Atoi(args[1])
						datastore.PaymentAdd(datastore.Payment{
							AdID:    ad.ID,
							Amount:  ad.Amount,
							Method:  datastore.MethodT(method),
							Created: time.Now(),
						})
						ModerateAd(ad)
					}
				case "balance":
					u, _ := datastore.UserGetBy("id", arg)
					AddMsgI(AdminChat, fmt.Sprintf("Баланс <a href=\"tg://user?id=%d\">пользователя</a> %sр.", u.ID, u.Balance.String()))
				case "msg":
					//distributionFree(arg)
				default:
				}
			}
		} //ADMIN
	}
}
func somethingWentWrong(chatID int64) {
	AddMsg(chatID, "Что-то пошло не так!")
}
func senderStart() {
	rl := ratelimit.New(30)

	for {
		rl.Take()
		canSend <- true
		//msg := <-Bot.Msgs
		//sendMsg(msg)
	}
}
func AddMsg(chatID int64, text string) (tgbotapi.Message, bool) {
	return sendMsg(Msg{
		Text:   text,
		ChatID: chatID,
		Markup: nil,
	})
}
func AddMsgI(chatID int, text string) (tgbotapi.Message, bool) {
	return AddMsg(int64(chatID), text)
}

func AddMsgM(chatID int64, text string, markup interface{}) (tgbotapi.Message, bool) {
	return sendMsg(Msg{
		Text:   text,
		ChatID: chatID,
		Markup: markup,
	})
}
func AddMsgMI(chatID int, text string, markup interface{}) (tgbotapi.Message, bool) {
	return AddMsgM(int64(chatID), text, markup)
}
func sendMsg(m Msg) (tgbotapi.Message, bool) {
	msg := tgbotapi.NewMessage(m.ChatID, m.Text)
	msg.ParseMode = tgbotapi.ModeHTML
	if m.Markup != nil {
		msg.ReplyMarkup = m.Markup
	}
	tm, err := send(msg)

	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err, "text": m.Text, "id": m.ChatID, "markup": m.Markup}).Error("Error in SendMsg when execute bot.Send (telegram). ")
		return tm, false
	}
	return tm, true
}

//func sendAdMsg(msg tgbotapi.MessageConfig) bool {
//	_, err := send(msg)
//	if err != nil && err.Error() == "Forbidden: bot was blocked by the user" {
//		go deactivate(msg.ChatID)
//		return false
//	}
//	return true
//}
func send(c tgbotapi.Chattable) (tgbotapi.Message, error) {
	<-canSend
	return Bot.Send(c)
}

func SendAdvMsg(id int, text string) bool {
	return SendAdvMsgI(int64(id), text)
}
func SendAdvMsgI(id int64, text string) bool {
	return sendAdvMsg(id, text, nil)
}
func SendAdvMsgM(id int, text string, markup interface{}) bool {
	return sendAdvMsg(int64(id), text, markup)
}
func sendAdvMsg(id int64, text string, markup interface{}) bool {
	msg := tgbotapi.NewMessage(id, text)
	msg.ParseMode = tgbotapi.ModeHTML

	if markup != nil {
		msg.ReplyMarkup = markup
	}
	_, err := AdvBot.Send(msg)
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err, "text": text, "id": id, "markup": markup}).Error("Error in sendAdvMsg when execute bot.Send (telegram). ")
		return false
	}
	return true
}
func AddBalanceWithNotification(bot datastore.Bot, add decimal.Decimal) {
	datastore.AddBalance(bot.OwnerID, add)
	AddMsgI(bot.OwnerID, fmt.Sprintf("@%s заработал <b>%s</b>", bot.Username, MoneyAC.FormatMoneyDecimal(add)))
	//AddMsgI(AdminChat, fmt.Sprintf("<b>ID: %d</b>\n"+
	//	"У <a href=\"tg://user?id=%d\">пользователя</a> баланс пополнен на %sр.", userID, userID, add.String()))
}

// func collectAndSendAnalytics() {
// 	gocron.ChangeLoc(location)
// 	gocron.Every(1).Day().At("00:01").Do(analytics.AddAnalytics)
// 	gocron.Every(1).Day().At("23:59").Do(analytics.CollectAnalytics)
// 	gocron.Every(1).Day().At("8:00").Do(sendAnalytics)
// 	<-gocron.Start()
// }
