package telegram

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"../storage/datastore"
	"../utility"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/satori/go.uuid"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

const (
	cycle                 = 90 * time.Minute
	fallAsleepAverageTime = 14 * time.Minute
)

var (
	mKeyboard       = tgbotapi.NewInlineKeyboardMarkup
	mKeyboardRow    = tgbotapi.NewInlineKeyboardRow
	mKeyboardBtnURL = tgbotapi.NewInlineKeyboardButtonURL
	HideBtn         = mKeyboard(mKeyboardRow(makeIB("Скрыть", "", cHide)))
)

// func format(hour, minute int) string {
// 	var hourZero, minuteZero string
// 	if hour < 10 {
// 		hourZero = "0"
// 	}
// 	if minute < 10 {
// 		minuteZero = "0"
// 	}
// 	return fmt.Sprintf("%s%d:%s%d", hourZero, hour, minuteZero, minute)
// }

// func sendAnalytics() {
// 	AddMsg(145771050, analytics.GetAnalytics())
// }

// func rnd(k float64) bool {
// 	return rand.Intn(100) < int(k*100)
// }

// func shareMsg(chatID int64) {
// 	if rnd(0.1) {
// 		AddMsg(chatID,
// 			"Если наш бот полезен для вас, пожалуйтса расскажите о нём вашим друзьям, это помогает нам развиваться, благодарим вас!")
// 	}
// }

func PayMsg(user, id int, a decimal.Decimal) {
	amount := a.StringFixed(2)
	i := strings.Index(amount, ".")
	keyboard := mKeyboard(
		mKeyboardRow(
			mKeyboardBtnURL("Любой банковской картой (Яндекс.Деньги)", ymLink("AC", amount, user)),
		),
		mKeyboardRow(
			mKeyboardBtnURL("Яндекс.Деньги", ymLink("PC", amount, user)),
		),
		mKeyboardRow(
			mKeyboardBtnURL("Qiwi (или банковская карта)", fmt.Sprintf("https://w.qiwi.com/payment/form/99?currency=643&amountInteger=%s&amountFraction=%s&extra['account']=79287750060&extra['comment']=%d", amount[:i], amount[i+1:], id)),
		),
		mKeyboardRow(
			mKeyboardBtnURL("Веб-страница оплаты", fmt.Sprintf("http://ltest.dd/#/pay/%d", id)),
		),
	)
	SendAdvMsgM(user, fmt.Sprintf("<b>Оплаты заказа #%d</b>\n\n"+
		"<b>Картой Сбербанк</b>\n"+
		"Номер карты: <code>4274 3200 2822 4643</code>\n"+
		"Сумма: <code>%s</code> руб.\n"+
		"Комментарий: <code>%d</code>\n\n"+
		"<b>Картой Тинькофф</b>\n"+
		"Номер карты: <code>5536 9138 0794 8464</code>\n"+
		"Сумма: <code>%s</code> руб.\n"+
		"Комментарий: <code>%d</code>\n", id, amount, id, amount, id), keyboard)
}
func ymLink(t, amount string, user int) string {
	return fmt.Sprintf("https://money.yandex.ru/transfer?receiver=410012501900438&sum=%s&successURL=https://tele.click/messageadvbot&quickpay-back-url=https://tele.click/messageadvbot&shop-host=money.yandex.ru&label=%d&targets=MessageAd&comment=%d&origin=button&selectedPaymentType=%s&destination=MessageAd&form-comment=%d&short-dest=&quickpay-form=small", amount, user, user, t, user)
}
func checkUser(id int, username string) datastore.User {
	user, ok := datastore.UserGetBy("id", id)
	if user.IsEmpty() || !ok {
		user = datastore.User{
			ID:       id,
			Username: username,
			Balance:  decimal.Zero,
			Created:  time.Now(),
		}
		datastore.UserAdd(user)
		AddMsgMI(AdminChat, fmt.Sprintf("<b>Новый ad пользователь</b>\n"+
			"ID: <a href=\"tg://user?id=%d\">%d</a>\n"+
			"Username: @%s", id, id, username), HideBtn)

	}
	if user.Username != username {
		datastore.UserEditOne("id", user.ID, "username", username)
	}
	return user
}
func checkAdvUser(id int, username string) datastore.AdvUser {
	user, ok := datastore.AdvUserGetBy("id", id)
	if user.IsEmpty() || !ok {
		user = datastore.AdvUser{
			ID:       id,
			Username: username,
			Balance:  decimal.Zero,
			Token:    uuid.Must(uuid.NewV4()),
			Created:  time.Now(),
		}
		datastore.AdvUserAdd(user)
		AddMsgMI(AdminChat, fmt.Sprintf("<b>Новый adv пользователь</b>\n"+
			"ID: <a href=\"tg://user?id=%d\">%d</a>\n"+
			"Username: @%s", id, id, username), HideBtn)
	}
	if user.Username != username {
		datastore.UserEditOne("id", user.ID, "username", username)
	}
	return user
}
func ModerateAd(ad datastore.AdT) {
	datastore.AdEditOne("id", ad.ID, "stage", datastore.Payed)
	//ad, _ := datastore.AdGetBy("id", id)
	var dType string
	switch ad.T {
	case datastore.DAll:
		dType = "по всем"
	case datastore.DCount:
		dType = "по количеству"
	case datastore.DBot:
		dType = "по боту"
	}
	m, _ := AddMsgMI(AdminChat, ad.Text, HideBtn)
	approveAdButton := mKeyboardRow(makeIB("Принять", fmt.Sprintf("%d %d", ad.ID, m.MessageID), cAdApprove))
	blockAdButton := mKeyboardRow(makeIB("Отклонить", fmt.Sprintf("%d %d", ad.ID, m.MessageID), cAdBlock))
	AddMsgMI(AdminChat, fmt.Sprintf("<b>Реклама #%d</b>\n"+
		"Стоимость рекламы: %s\n"+
		"Тип рассылки: %s\n"+
		"Количетсво сообщений: %s\n"+
		"Рекламу заказал <a href=\"tg://user?id=%d\">пользователь</a>", ad.ID, ad.Amount.String(), dType, ac(ad.Count), ad.OwnerID), mKeyboard(approveAdButton, blockAdButton))
	SendAdvMsg(ad.OwnerID, fmt.Sprintf("Реклама #%d оплачена и отправлена на модерацию.", ad.ID))
}
func makeIB(text, data string, t CallbackType) tgbotapi.InlineKeyboardButton { //makeInlineButton
	return tgbotapi.NewInlineKeyboardButtonData(text, strconv.Itoa(int(t))+" "+data)
}

// func mKeyboard(rows ...[]tgbotapi.InlineKeyboardButton) tgbotapi.InlineKeyboardMarkup {
// 	return mKeyboard(rows...)
// }
func webMsg(id int64, token uuid.UUID) {
	msg := tgbotapi.NewMessage(id, "<b>MessageAd</b>\n\n"+
		"Нажмите на кнопку ниже, чтобы открыть MessageAd и заказать рекламу через браузер.\n\n"+
		"Отправьте /web, чтобы получить новую ссылку(старая перестанет работать).")
	msg.ParseMode = tgbotapi.ModeHTML
	msg.ReplyMarkup = mKeyboard(mKeyboardRow(mKeyboardBtnURL("Открыть", fmt.Sprintf("http://messagead.co/#/newpost?token=%s", token))))
	_, err := AdvBot.Send(msg)
	utility.CheckErr(err)
}
func adStartMsg(id, owner int) {
	SendAdvMsg(owner, fmt.Sprintf("<i>Начало рекламной рассылки #%d.</i>", id))
}
func adSuccessMsg(id, owner int) {
	SendAdvMsg(owner, fmt.Sprintf("<i>Рекламная рассылка #%d завершена.</i>", id))
}

//func deactivate(b,id int) {
//	datastore.UserEditOne("id", id, "is_active", false)
//}
func withdraw(arg string, user datastore.User, method datastore.MethodT) {
	if arg != "" {
		args := strings.Split(arg, " ")
		var withdrawAmount decimal.Decimal
		switch len(args) {
		case 1:
			withdrawAmount = user.Balance
		case 2:
			withdrawAmount = decimal.RequireFromString(args[1])
		default:
			AddMsgI(user.ID, "Вы указали слишком большое число аргументов")
		}
		if withdrawAmount.LessThanOrEqual(user.Balance) {
			if withdrawAmount.GreaterThanOrEqual(minimumWithdraw) {
				withdraw := datastore.Withdraw{
					UserID:  user.ID,
					Amount:  withdrawAmount,
					Method:  method,
					Comment: args[0],
					Created: time.Now(),
				}
				_, ok := datastore.WithdrawAdd(withdraw)
				if ok {
					formattedAmount := MoneyAC.FormatMoneyDecimal(withdraw.Amount)
					AddMsgI(user.ID, fmt.Sprintf("Вывод средств будет осуществлен в течении одного рабочего дня!\nПри необходимости с вам свяжется наш менеджер!\n\n"+
						"<b>Сумма:</b> %s\n"+
						"<b>Кошелек:</b> %s\n", formattedAmount, withdraw.Comment))
					datastore.UserEditOne("id", user.ID, "balance", user.Balance.Sub(withdrawAmount))
					var withdrawStr string
					switch method {
					case datastore.YM:
						withdrawStr = "Яндекс.Деньги"
						YmAddBalance(withdraw.Comment, withdraw.Amount)
					case datastore.Qiwi:
						withdrawStr = "Qiwi"
						QiwiAddBalance(withdraw.Comment, withdraw.Amount)
					}
					AddMsgI(AdminChat, fmt.Sprintf("<b>Вывод средств на %s</b>\n\n"+
						"<b>Сумма:</b> %s\n"+
						"<b>Кошелек:</b> %s", withdrawStr, formattedAmount, withdraw.Comment))
				} else {
					AddMsgI(user.ID, "Что-то пошло не так. Попробуйте позже или обратитесь в поддержку.")
				}
			} else {
				AddMsgI(user.ID, "Для вывода средств необходимо чтобы на балансе было более 100 рублей!")
			}
		} else {
			AddMsgI(user.ID, "У Вас недостаточно средств!")
		}
	} else {
		AddMsgI(user.ID, "Вы не ввели номер кошелька!")
	}
}

func GetAdFromCallback(data string, index int) (datastore.AdT, bool) {
	id, _ := strconv.Atoi(data[:index])
	return datastore.AdGetBy("id", id)
}
func GetBotFromCallback(data string, index int) (datastore.Bot, bool) {
	if index > 0 {
		data = data[:index]
	}
	id, _ := strconv.Atoi(data)
	return datastore.BotGetBy("id", id)
}
func formatAdMsg(str string) string {
	//str = strings.Replace(str, "<p>", "", -1)
	//str = strings.Replace(str, "</p>", "", -1)
	//str = strings.Replace(str, "<br>", "\n", -1)
	//str = strings.Replace(str, " target=\"_blank\">", ">", -1)
	//str = strings.Replace(str, "<pre class=\"ql-syntax\" spellcheck=\"false\">", "<pre>", -1)
	//str =
	return str + "\n\n" +
		"Реклама через @MessageAd"
}
func statsUpdater() {
	var statMsgID int

	msg := tgbotapi.NewMessage(adminChat64, statMsg())
	msg.ParseMode = tgbotapi.ModeHTML
	msg.ReplyMarkup = HideBtn
	m, err := send(msg)
	if err == nil {
		statMsgID = m.MessageID
	} else {
		logrus.Error(err)
	}

	ticker := time.NewTicker(time.Minute * 2)
	for range ticker.C {
		if statMsgID != 0 {
			m := tgbotapi.NewEditMessageText(adminChat64, statMsgID, statMsg())
			m.ParseMode = tgbotapi.ModeHTML
			m.ReplyMarkup = &HideBtn
			Bot.Send(m)
		}
	}
}
func statMsg() string {
	now := time.Now()
	telegramUsersActiveCount, _ := datastore.TelegramUsersActiveCount()
	telegramUsersCount, _ := datastore.TelegramUsersCount()

	botUsersActiveCount, _ := datastore.BotUsersOverallActiveCount()
	botUsersCount, _ := datastore.BotUsersOverallCount()

	AdUsersCount, _ := datastore.UsersCount()
	AdvUsersCount, _ := datastore.AdvUsersCount()

	BotsCount, _ := datastore.BotsCount()

	AdsCount, _ := datastore.AdsCount()
	AdsApprovedCount, _ := datastore.AdsByStageCount(datastore.Approved)
	AdsDStartCount, _ := datastore.AdsByStageCount(datastore.DStart)

	var distributionIsOn string

	if AdsDStartCount > 0 {
		distributionIsOn = "✅"
	} else {
		distributionIsOn = "⭕"
	}

	return fmt.Sprintf("<b>Пользователи</b>\n"+
		"- Уникальные: %s (%s)\n"+
		"- Неуникальные: %s (%s)\n\n"+
		"<b>Пользователи MessageAd</b>\n"+
		"- Владельцы ботов: %s\n"+
		"- Рекламодатели: %s\n\n"+
		"<b>Боты</b>\n"+
		"- Всего: %s\n\n"+
		"<b>Реклама</b>\n"+
		"- Всего: %s\n"+
		"- Ожидают: %s\n"+
		"- Рассылаются: %s\n"+
		"- Статус рассылки: %s\n\n"+
		"Время потраченное на сбор статистики: %d сек.\n"+
		"Время обновления: %s", ac(telegramUsersActiveCount), ac(telegramUsersCount), ac(botUsersActiveCount), ac(botUsersCount), ac(AdUsersCount), ac(AdvUsersCount), ac(BotsCount), ac(AdsCount), ac(AdsApprovedCount), ac(AdsDStartCount), distributionIsOn, int(time.Since(now).Seconds()), time.Now().In(location).String())
}
func adTotalMsg(ad datastore.AdT, invoice decimal.Decimal) {
	AddMsgMI(AdminChat, fmt.Sprintf("<b>Итог рекламной рассылки #%d.</b>\n"+
		"Сумма: <b>%s</b>\n"+
		"Стоимость: <b>%s</b>\n"+
		"Заработано: <b>%s</b>", ad.ID, MoneyAC.FormatMoneyDecimal(ad.Amount), MoneyAC.FormatMoneyDecimal(invoice), MoneyAC.FormatMoneyDecimal(ad.Amount.Sub(invoice))), HideBtn)
}

//func IsBotOwner(from int, data string) bool {
//	botID, _ := strconv.Atoi(data)
//	bot, ok := datastore.BotGetBy("id", botID)
//	if ok {
//		if bot.OwnerID == from {
//			return true
//		} else {
//			AddMsgI(from, "Вы не имеете доступа к этому боту!")
//		}
//	} else {
//		AddMsgI(from, "Такого бота не существует!")
//	}
//	return false
//}

func QiwiAddBalance(account string, amount decimal.Decimal) {
	a, _ := amount.Round(2).Float64()
	data := PayloadQiwi{
		ID: strconv.Itoa(int(time.Now().UnixNano())),
		Sum: Sum{
			Amount:   a,
			Currency: "643",
		},
		PaymentMethod: PaymentMethod{
			Type:      "Account",
			AccountID: "643",
		},
		Comment: "",
		Fields: Fields{
			Account: account,
		},
	}
	payloadBytes, err := json.Marshal(data)
	utility.CheckErr(err)
	body := bytes.NewReader(payloadBytes)

	req, err := http.NewRequest("POST", "https://edge.qiwi.com/sinap/api/v2/terms/99/payments", body)
	utility.CheckErr(err)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Bearer f33e27692281bf7caf51b619afd96063")

	resp, err := http.DefaultClient.Do(req)
	utility.CheckErr(err)
	if resp != nil {
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		utility.Check("QiwiAddBalance", err)
		logrus.Info("Qiwi Payment")
		logrus.Info(string(body))
		var data QiwiAnswer
		err = json.Unmarshal(body, &data)

		if err == nil {
			switch data.Transaction.State.Code {
			case "Accepted":
				logrus.Infof("Выплата на %s через Qiwi принята.", account)
			default:
				AddMsgI(AdminChat, fmt.Sprintf("Произошла ошибка при попытке вывести средства на Qiwi\n"+
					"Статус: %s\n"+
					"Сумма: %sр\n"+
					"Кошелек: %s",
					data.Transaction.State.Code, amount.String(), account))
			}
		} else {
			logrus.Error(err)
		}
	}
}

func YmAddBalance(account string, amount decimal.Decimal) {

	//q := req.URL.Query()
	//q.Add("pattern_id", "p2p")
	//q.Add("to", account)
	//q.Add("amount", amount.StringFixed(2))
	data := url.Values{}
	data.Set("pattern_id", "p2p")
	data.Set("to", account)
	data.Set("amount", amount.StringFixed(2))

	req, err := http.NewRequest("POST", "https://money.yandex.ru/api/request-payment", strings.NewReader(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", "Bearer 410012501900438.B4FB66EDE62ABDCA0FD849F5D830475AE04A16EF55412D94EE989907F233AFB6F3498BCF33E2C48501FE350D007E98FF8A5ABD7A6804B15819C9375D23C62DD2C55FE2162B90DFAF173D8F9EBE24A096B59ED321AB06D15BDF0DD4291E16E5D449F4AEAA9B3EF53B9140B81AD47CAC277C44078B23293FA503B9747A9EC8EF0C")
	req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))
	resp, err := http.DefaultClient.Do(req)
	utility.Check("YmAddBalance", err)

	if resp != nil {
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)

		utility.Check("YmAddBalance", err)
		logrus.Info("Yandex Money Payment")
		logrus.Info(string(body))
		var data YMPayment
		err = json.Unmarshal(body, &data)

		if err == nil {
			switch data.Status {
			case "success":
				logrus.Infof("Выплата на %s через Яндекс.Деньги принята.", account)
				YmProcess(data.RequestID)
			default:
				AddMsgI(AdminChat, fmt.Sprintf("Произошла ошибка при попытке вывести средства на Яндекс.Деньги\n"+
					"Статус: %s\n"+
					"Ошибка: %s\n"+
					"Сумма: %sр\n"+
					"Кошелек: %s",
					data.Status, data.Error, amount.String(), account))
			}
		} else {
			logrus.Error(err)
		}
	}
}

func YmProcess(request string) {

	//q := req.URL.Query()
	//q.Add("pattern_id", "p2p")
	//q.Add("to", account)
	//q.Add("amount", amount.StringFixed(2))
	data := url.Values{}
	data.Set("request_id", request)

	req, err := http.NewRequest("POST", "https://money.yandex.ru/api/process-payment", strings.NewReader(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", "Bearer 410012501900438.B4FB66EDE62ABDCA0FD849F5D830475AE04A16EF55412D94EE989907F233AFB6F3498BCF33E2C48501FE350D007E98FF8A5ABD7A6804B15819C9375D23C62DD2C55FE2162B90DFAF173D8F9EBE24A096B59ED321AB06D15BDF0DD4291E16E5D449F4AEAA9B3EF53B9140B81AD47CAC277C44078B23293FA503B9747A9EC8EF0C")
	resp, err := http.DefaultClient.Do(req)
	utility.Check("YmProcess", err)

	if resp != nil {
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		utility.Check("YmAddBalance", err)
		logrus.Info("Yandex Money Payment")
		logrus.Info(string(body))
	}
}
