package analytics

import (
	"../storage/datastore"
	"fmt"
	"github.com/jasonlvhit/gocron"
	"sync"
	"time"
)

var (
	location, _ = time.LoadLocation("Europe/Moscow")
	mu          = &sync.Mutex{}
	uses        = 0
	lastID      = 0
)

func Start() {
	checkIfAnalyticsAdded()
	gocron.ChangeLoc(location)
	gocron.Every(1).Day().At("00:01").Do(AddAnalytics)
	gocron.Every(1).Day().At("23:59").Do(CollectAnalytics)
	go mergeUses()
}
func checkIfAnalyticsAdded() {
	t := time.Now().In(location)
	lastStat, ok := datastore.StatGetLast()

	if ok && !lastStat.IsEmpty() {
		if t.Sub(lastStat.Created).Hours() > 24 {
			AddAnalytics()
		} else {
			lastID = lastStat.ID
		}

	} else {
		AddAnalytics()
	}

}
func AddAnalytics() {
	t := time.Now().In(location)
	id, _ := datastore.StatAdd(datastore.Stat{
		Created: t,
	})
	lastID = id
}
func CollectAnalytics() {
	count, ok := datastore.UsersCount()
	if ok {
		datastore.StatEditOne("id", lastID, "users", count)
	}
}

func GetAnalytics() string {
	stat, ok := datastore.StatGetBy("id", lastID-1)
	if ok {
		return fmt.Sprintf("Количество пользователей: %d\n"+
			"Количество использований: %d", stat.Users, stat.Uses)
	}
	return ""
}
func GetAnalyticsPermanent() string {
	count, ok := datastore.UsersCount()
	stat, ok2 := datastore.StatGetLast()
	if ok && ok2 {
		return fmt.Sprintf("Количество пользователей: %d\n"+
			"Количество использований: %d", count, stat.Uses)
	}
	return ""
}

func AddUses() {
	mu.Lock()
	uses++
	mu.Unlock()
}
func mergeUses() {
	ticker := time.NewTicker(1 * time.Second)

	for range ticker.C {
		mu.Lock()
		us := uses
		uses = 0
		mu.Unlock()
		count := 0
		datastore.StatGetByColumnOne("id", lastID, "uses", &count)
		datastore.StatEditOne("id", lastID, "uses", count+us)
	}
}
