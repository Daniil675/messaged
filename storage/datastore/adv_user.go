package datastore

import (
	"reflect"
)

func AdvUserAdd(u AdvUser) (int, bool) {
	return Datastore.Add("adv_users", u)
}

func AdvUserGetBy(key, value interface{}) (AdvUser, bool) {
	var user AdvUser
	ok := Datastore.GetBy("adv_users", &user, key, value)
	return user, ok
}
func AdvUserGetByColumnOne(key, value, column, i interface{}) bool {
	return Datastore.GetByColumnOne("adv_users", key, value, column, i)
}
func AdvUsersGetsBy(key, value interface{}) ([]AdvUser, bool) {
	var user []AdvUser
	ok := Datastore.GetsBy("adv_users", &user, key, value)
	return user, ok
}
func AdvUserEdit(u AdvUser) bool {
	_, ok := AdvUserGetBy("id", u.ID)
	if ok {
		return Datastore.EditBy("adv_users", u, "id", u.ID)
	}
	return false
}
func AdvUserEditOne(key, value, field, i interface{}) bool {
	return Datastore.EditOne("adv_users", key, value, field, i)
}
func AdvUsersGetAll() ([]AdvUser, bool) {
	var i []AdvUser
	ok := Datastore.GetAll("adv_users", &i)
	return i, ok
}

func AdvUsersCount() (int, bool) {
	count, ok := Datastore.Count("adv_users")
	return count, ok
}

func (u *AdvUser) IsEmpty() bool { return reflect.DeepEqual(AdvUser{}, u) }
