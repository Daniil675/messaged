package datastore

import (
	"github.com/sirupsen/logrus"
	"upper.io/db.v3/postgresql"
)

var (
	Datastore *DatastoreT
)

func init() {
	Datastore = new(DatastoreT)
}
func New(settings postgresql.ConnectionURL) {
	sess, err := postgresql.Open(settings)
	if err != nil {
		logrus.Error("Error in New.", err)
	}
	sess.SetLogging(false)
	sess.SetMaxOpenConns(1000)

	Datastore = &DatastoreT{Database: sess}
}
