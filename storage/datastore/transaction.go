package datastore

import (
	"reflect"
	"time"
	"upper.io/db.v3"
)

func TransactionAdd(i Transaction) (int, bool) {
	return Datastore.Add("transactions", i)
}

func TransactionGetBy(key, value interface{}) (Transaction, bool) {
	var i Transaction
	ok := Datastore.GetBy("transactions", &i, key, value)
	return i, ok
}
func TransactionGetByColumnOne(key, value, column, i interface{}) bool {
	return Datastore.GetByColumnOne("transactions", key, value, column, i)
}
func TransactionsGetsBy(key, value interface{}) ([]Transaction, bool) {
	var i []Transaction
	ok := Datastore.GetsBy("transactions", &i, key, value)
	return i, ok
}
func TransactionsGetsByDate(date time.Time) ([]Transaction, bool) {
	var i []Transaction
	ok := Datastore.GetsByCond("transactions", &i, db.Cond{"created": db.OnOrAfter(date)})
	return i, ok
}
func TransactionEdit(i Transaction) bool {
	_, ok := TransactionGetBy("id", i.ID)
	if ok {
		return Datastore.EditBy("transactions", i, "id", i.ID)
	}
	return false
}
func TransactionEditOne(key, value, field, i interface{}) bool {
	return Datastore.EditOne("transactions", key, value, field, i)
}
func TransactionsGetAll() ([]Transaction, bool) {
	var i []Transaction
	ok := Datastore.GetAll("transactions", &i)
	return i, ok
}

func TransactionsCount() (int, bool) {
	count, ok := Datastore.Count("transactions")
	return count, ok
}

func (i *Transaction) IsEmpty() bool { return reflect.DeepEqual(Transaction{}, i) }
