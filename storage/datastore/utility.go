package datastore

import (
	"github.com/shopspring/decimal"
	"time"
	"upper.io/db.v3"
)

func UserToBotAdd(userID, botID int) bool {
	t := time.Now() //.Add(-time.Hour * 24 * 365)
	if tUser, ok := TelegramUserGetBy("id", userID); !ok {
		TelegramUserAdd(TelegramUser{
			ID:       userID,
			IsActive: true,
			LastMsg:  t,
		})
	} else if !tUser.IsActive {
		TelegramUserEditOne("id", userID, "is_active", true)
	}

	if bUser, ok := BotUserGetBy(userID, botID); !ok {
		BotUserAdd(BotUser{
			UserID:   userID,
			BotID:    botID,
			IsActive: true,
			LastMsg:  t,
		})
		return true
	} else if !bUser.IsActive {
		BotUserEditOne("id", userID, "is_active", true)
		return true
	}
	return false
}

func AddBalance(userID int, add decimal.Decimal) {
	var balance decimal.Decimal
	UserGetByColumnOne("id", userID, "balance", &balance)
	UserEditOne("id", userID, "balance", balance.Add(add))
}
func AddAdvBalance(userID int, add decimal.Decimal) {
	var balance decimal.Decimal
	UserGetByColumnOne("id", userID, "balance", &balance)
	AdvUserEditOne("id", userID, "balance", balance.Add(add))
}
func BotUserEditLastMsg(userID, botID int) bool {
	now := time.Now()
	return BotUserEditByCond("last_msg", now, db.Cond{"user_id": userID, "bot_id": botID})
}
func BotUserDeactivate(userID, botID int) bool {
	ok := BotUserEditByCond("is_active", false, db.Cond{"user_id": userID, "bot_id": botID})
	if ok {
		bots, ok := BotUserGetsByUserActive(userID)
		if ok && len(bots) == 0 {
			TelegramUserEditOne("id", userID, "is_active", false)
		}
	}
	return ok
}

func BotDelete(id int) {
	Datastore.DeleteBy("bots", db.Cond{"id": id})
	Datastore.DeleteBy("bot_users", db.Cond{"bot_id": id})
}
