package datastore

import (
	"reflect"
	"time"
	"upper.io/db.v3"
)

func PaymentAdd(i Payment) (int, bool) {
	return Datastore.Add("payments", i)
}

func PaymentGetBy(key, value interface{}) (Payment, bool) {
	var i Payment
	ok := Datastore.GetBy("payments", &i, key, value)
	return i, ok
}
func PaymentGetByColumnOne(key, value, column, i interface{}) bool {
	return Datastore.GetByColumnOne("payments", key, value, column, i)
}
func PaymentsGetsBy(key, value interface{}) ([]Payment, bool) {
	var i []Payment
	ok := Datastore.GetsBy("payments", &i, key, value)
	return i, ok
}
func PaymentsGetsByDate(date time.Time) ([]Payment, bool) {
	var i []Payment
	ok := Datastore.GetsByCond("payments", &i, db.Cond{"created": db.OnOrAfter(date)})
	return i, ok
}
func PaymentEdit(i Payment) bool {
	_, ok := PaymentGetBy("id", i.ID)
	if ok {
		return Datastore.EditBy("payments", i, "id", i.ID)
	}
	return false
}
func PaymentEditOne(key, value, field, i interface{}) bool {
	return Datastore.EditOne("payments", key, value, field, i)
}
func PaymentsGetAll() ([]Payment, bool) {
	var i []Payment
	ok := Datastore.GetAll("payments", &i)
	return i, ok
}

func PaymentsCount() (int, bool) {
	count, ok := Datastore.Count("payments")
	return count, ok
}

func (i *Payment) IsEmpty() bool { return reflect.DeepEqual(Payment{}, i) }
