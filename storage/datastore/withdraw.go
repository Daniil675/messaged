package datastore

import (
	"reflect"
)

func WithdrawAdd(i Withdraw) (int, bool) {
	return Datastore.Add("withdraws", i)
}

func WithdrawGetBy(key, value interface{}) (Withdraw, bool) {
	var withdraw Withdraw
	ok := Datastore.GetBy("withdraws", &withdraw, key, value)
	return withdraw, ok
}
func WithdrawGetByColumnOne(key, value, column, i interface{}) bool {
	return Datastore.GetByColumnOne("withdraws", key, value, column, i)
}
func WithdrawsGetsBy(key, value interface{}) ([]Withdraw, bool) {
	var withdraw []Withdraw
	ok := Datastore.GetsBy("withdraws", &withdraw, key, value)
	return withdraw, ok
}
func WithdrawEdit(i Withdraw) bool {
	_, ok := WithdrawGetBy("id", i.ID)
	if ok {
		return Datastore.EditBy("withdraws", i, "id", i.ID)
	}
	return false
}
func WithdrawEditOne(key, value, field, i interface{}) bool {
	return Datastore.EditOne("withdraws", key, value, field, i)
}
func WithdrawsGetAll() ([]Withdraw, bool) {
	var withdraws []Withdraw
	ok := Datastore.GetAll("withdraws", &withdraws)
	return withdraws, ok
}

func WithdrawsCount() (int, bool) {
	count, ok := Datastore.Count("withdraws")
	return count, ok
}

func (i *Withdraw) IsEmpty() bool { return reflect.DeepEqual(Withdraw{}, i) }
