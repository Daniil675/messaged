package datastore

import (
	"reflect"
)

func BotAdd(item Bot) (int, bool) {
	return Datastore.Add("bots", item)
}

func BotGetBy(key, value interface{}) (Bot, bool) {
	var item Bot
	ok := Datastore.GetBy("bots", &item, key, value)
	return item, ok
}
func BotGetByColumnOne(key, value, column, item interface{}) bool {
	return Datastore.GetByColumnOne("bots", key, value, column, item)
}
func BotsGetsBy(key, value interface{}) ([]Bot, bool) {
	var item []Bot
	ok := Datastore.GetsBy("bots", &item, key, value)
	return item, ok
}
func BotEdit(item Bot) bool {
	_, ok := BotGetBy("id", item.ID)
	if ok {
		return Datastore.EditBy("bots", item, "id", item.ID)
	}
	return false
}
func BotEditOne(key, value, field, item interface{}) bool {
	return Datastore.EditOne("bots", key, value, field, item)
}
func BotsGetAll() ([]Bot, bool) {
	var bots []Bot
	ok := Datastore.GetAll("bots", &bots)
	return bots, ok
}

func BotsCount() (int, bool) {
	count, ok := Datastore.Count("bots")
	return count, ok
}

func (i *Bot) IsEmpty() bool { return reflect.DeepEqual(Bot{}, i) }
