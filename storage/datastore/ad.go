package datastore

import (
	"reflect"
	"time"
	"upper.io/db.v3"
)

func AdAdd(i AdT) (int, bool) {
	return Datastore.Add("ads", i)
}

func AdGetBy(key, value interface{}) (AdT, bool) {
	var user AdT
	ok := Datastore.GetBy("ads", &user, key, value)
	return user, ok
}
func AdGetByColumnOne(key, value, column, i interface{}) bool {
	return Datastore.GetByColumnOne("ads", key, value, column, i)
}
func AdsGetsBy(key, value interface{}) ([]AdT, bool) {
	var user []AdT
	ok := Datastore.GetsBy("ads", &user, key, value)
	return user, ok
}
func AdsGetsByDate(date time.Time) ([]AdT, bool) {
	var user []AdT
	ok := Datastore.GetsByCond("ads", &user, db.Cond{"created": db.OnOrAfter(date)})
	return user, ok
}
func AdsGetsDCountByCreated(limit int) ([]AdT, bool) {
	var user []AdT
	ok := Datastore.GetByCondOrderLimit("ads", &user, db.Cond{"stage": Approved, "t": DCount}, "created", limit)
	return user, ok
}
func AdEdit(u AdT) bool {
	_, ok := AdGetBy("id", u.ID)
	if ok {
		return Datastore.EditBy("ads", u, "id", u.ID)
	}
	return false
}
func AdEditOne(key, value, field, i interface{}) bool {
	return Datastore.EditOne("ads", key, value, field, i)
}
func AdsGetAll() ([]AdT, bool) {
	var ads []AdT
	ok := Datastore.GetAll("ads", &ads)
	return ads, ok
}

func AdsCount() (int, bool) {
	count, ok := Datastore.Count("ads")
	return count, ok
}
func AdsByStageCount(s StageT) (int, bool) {
	return Datastore.CountByCond("ads", db.Cond{"stage": s})
}

//func AdsApprovedCount() (int, bool) {
//	count, ok := Datastore.CountByCond("ads", db.Cond{"approved": true})
//	return count, ok
//}

func (u *AdT) IsEmpty() bool { return reflect.DeepEqual(AdT{}, u) }
