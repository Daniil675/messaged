package datastore

import (
	"../../utility"
	"reflect"
	"upper.io/db.v3"
)

func TelegramUserAdd(items TelegramUser) bool {
	return Datastore.AddWithoutID("telegram_users", items)
}

func TelegramUserGetBy(key, value interface{}) (TelegramUser, bool) {
	var items TelegramUser
	ok := Datastore.GetBy("telegram_users", &items, key, value)
	return items, ok
}
func TelegramUserGetByColumnOne(key, value, column, items interface{}) bool {
	return Datastore.GetByColumnOne("telegram_users", key, value, column, items)
}
func TelegramUsersGetsBy(key, value interface{}) ([]TelegramUser, bool) {
	var items []TelegramUser
	ok := Datastore.GetsBy("telegram_users", &items, key, value)
	return items, ok
}
func TelegramUsersGetsByCursor(isFirst bool, lastID, paginate int) []TelegramUser {
	var users []TelegramUser
	res := Datastore.GetCursor("telegram_users", "id", paginate, db.Cond{"is_active": true})

	if isFirst {
		_ = res.All(&users)
	} else {
		res = res.NextPage(lastID)
		_ = res.All(&users)
	}
	if err := res.Close(); err != nil {
		utility.Error(err, "TelegramUsersGetsByCursor")
	}
	return users
}
func TelegramUserEditOne(key, value, field, items interface{}) bool {
	return Datastore.EditOne("telegram_users", key, value, field, items)
}
func TelegramUsersGetAll() ([]TelegramUser, bool) {
	var items []TelegramUser
	ok := Datastore.GetAll("telegram_users", &items)
	return items, ok
}
func TelegramUsersGetsByLastMsg(limit int) ([]TelegramUser, bool) {
	var user []TelegramUser
	ok := Datastore.GetByCondOrderLimit("telegram_users", &user, db.Cond{"is_active": true}, "last_msg", limit)
	return user, ok
}
func TelegramUsersCount() (int, bool) {
	return Datastore.Count("telegram_users")
}
func TelegramUsersActiveCount() (int, bool) {
	return Datastore.CountByCond("telegram_users", db.Cond{"is_active": true})
}
func (i *TelegramUser) IsEmpty() bool { return reflect.DeepEqual(TelegramUser{}, i) }
