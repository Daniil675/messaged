package datastore

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/postgresql"
)

type (
	DatastoreT struct {
		sqlbuilder.Database
	}
	User struct {
		ID       int             `json:"id" db:"id,omitempty"`
		Username string          `json:"username" db:"username"`
		Balance  decimal.Decimal `json:"balance" db:"balance"`
		//Wallets  WalletT         `json:"wallets" db:"wallets"`
		Created time.Time `json:"created" db:"created"`
	}
	AdvUser struct {
		ID       int             `json:"id" db:"id,omitempty"`
		Username string          `json:"username" db:"username"`
		Balance  decimal.Decimal `json:"balance" db:"balance"`
		Token    uuid.UUID       `json:"token" db:"token"`
		Created  time.Time       `json:"created" db:"created"`
	}
	//WalletT struct {
	//	Card string `json:"card"`
	//	YM   string `json:"ym"`
	//	WM   string `json:"wm"`
	//	Qiwi string `json:"qiwi"`
	//	*postgresql.JSONBConverter
	//}
	Bot struct {
		ID       int    `json:"id" db:"id,omitempty"`
		Token    string `json:"token" db:"token"`
		ApiKey   string `json:"api_key" db:"api_key"`
		Username string `json:"username" db:"username"`
		Name     string `json:"name" db:"name"`
		OwnerID  int    `json:"owner_id" db:"owner_id"`
	}
	BotUser struct {
		UserID   int       `json:"user_id" db:"user_id,omitempty"`
		BotID    int       `json:"bot_id" db:"bot_id,omitempty"`
		IsActive bool      `json:"is_active" db:"is_active"`
		LastMsg  time.Time `json:"last_msg" db:"last_msg"`
	}
	TelegramUser struct {
		ID       int       `json:"id" db:"id,omitempty"`
		IsActive bool      `json:"is_active" db:"is_active"`
		LastMsg  time.Time `json:"last_msg" db:"last_msg"`
	}
	Stat struct {
		ID          int       `json:"id" db:"id,omitempty"`
		Users       int       `json:"users" db:"users"`
		ActiveUsers int       `json:"active_users" db:"active_users"`
		Uses        int       `json:"uses" db:"uses"`
		Created     time.Time `json:"created" db:"created"`
	}
	Transaction struct {
		ID      int             `json:"id" db:"id,omitempty"`
		BotID   int             `json:"bot_id" db:"bot_id"`
		Amount  decimal.Decimal `json:"amount" db:"amount"`
		T       DistributionT   `json:"t" db:"t"`
		Users   int             `json:"users" db:"users"`
		Created time.Time       `json:"created" db:"created"`
	}
	Withdraw struct {
		ID      int             `json:"id" db:"id,omitempty"`
		UserID  int             `json:"user_id" db:"user_id"`
		Amount  decimal.Decimal `json:"amount" db:"amount"`
		Method  MethodT         `json:"method"  db:"method"`
		Comment string          `json:"comment" db:"comment"`
		Created time.Time       `json:"created" db:"created"`
	}
	AdT struct {
		ID      int           `json:"id" db:"id,omitempty"`
		OwnerID int           `json:"owner_id" db:"owner_id"`
		Count   int           `json:"count" db:"count"` // -1 - All
		T       DistributionT `json:"t" db:"t"`
		Text    string        `json:"text" db:"text"`
		Img     string        `json:"img" db:"img"`
		//Buttons      postgresql.JSONBArray `json:"buttons" db:"buttons"`
		Amount  decimal.Decimal `json:"amount" db:"amount"`
		Invoice decimal.Decimal `json:"invoice" db:"invoice"`
		Stage   StageT          `json:"stage" db:"stage"`
		Created time.Time       `json:"created" db:"created"`
	}
	Button struct {
		Text string `json:"text"`
		Link string `json:"link"`
		*postgresql.JSONBConverter
	}
	Payment struct {
		ID      int             `json:"id" db:"id,omitempty"`
		AdID    int             `json:"ad_id" db:"ad_id,omitempty"`
		Amount  decimal.Decimal `json:"amount" db:"amount"`
		Method  MethodT         `json:"method"  db:"method"`
		Created time.Time       `json:"created" db:"created"`
	}
)
