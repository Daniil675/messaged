package datastore

import (
	"reflect"
)

func UserAdd(u User) (int, bool) {
	return Datastore.Add("users", u)
}

func UserGetBy(key, value interface{}) (User, bool) {
	var user User
	ok := Datastore.GetBy("users", &user, key, value)
	return user, ok
}
func UserGetByColumnOne(key, value, column, i interface{}) bool {
	return Datastore.GetByColumnOne("users", key, value, column, i)
}
func UsersGetsBy(key, value interface{}) ([]User, bool) {
	var user []User
	ok := Datastore.GetsBy("users", &user, key, value)
	return user, ok
}
func UserEdit(u User) bool {
	_, ok := UserGetBy("id", u.ID)
	if ok {
		return Datastore.EditBy("users", u, "id", u.ID)
	}
	return false
}
func UserEditOne(key, value, field, i interface{}) bool {
	return Datastore.EditOne("users", key, value, field, i)
}
func UsersGetAll() ([]User, bool) {
	var users []User
	ok := Datastore.GetAll("users", &users)
	return users, ok
}

func UsersCount() (int, bool) {
	count, ok := Datastore.Count("users")
	return count, ok
}

func (u *User) IsEmpty() bool { return reflect.DeepEqual(User{}, u) }
