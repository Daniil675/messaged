package datastore

type DistributionT int

const (
	_ = iota
	DAll
	DCount
	DBot
)

type StageT int

const (
	Created = iota
	PreModerate
	Payed
	Blocked
	Approved
	DStart
	DFinish
)

type MethodT int

const (
	No = iota
	Card
	YM
	Qiwi
	WM
)
