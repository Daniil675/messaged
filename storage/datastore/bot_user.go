package datastore

import (
	"reflect"
	"upper.io/db.v3"
)

func BotUserAdd(items BotUser) bool {
	return Datastore.AddWithoutID("bot_users", items)
}

//func BotUserGetBy(key, value interface{}) (BotUser, bool) {
//	var items BotUser
//	ok := Datastore.GetBy("bot_users", &items, key, value)
//	return items, ok
//}
func BotUserGetBy(userID, botID int) (BotUser, bool) {
	var items BotUser
	ok := Datastore.GetByCond("bot_users", &items, db.Cond{"user_id": userID, "bot_id": botID})
	return items, ok
}
func BotUserGetsByUserActive(userID int) ([]BotUser, bool) {
	var items []BotUser
	ok := Datastore.GetsByCond("bot_users", &items, db.Cond{"user_id": userID, "is_active": true})
	return items, ok
}
func BotUserGetByColumnOne(key, value, column, items interface{}) bool {
	return Datastore.GetByColumnOne("bot_users", key, value, column, items)
}
func BotUsersGetsBy(key, value interface{}) ([]BotUser, bool) {
	var items []BotUser
	ok := Datastore.GetsBy("bot_users", &items, key, value)
	return items, ok
}
func BotUserEditOne(key, value, field, items interface{}) bool {
	return Datastore.EditOne("bot_users", key, value, field, items)
}
func BotUserEditByCond(key, value interface{}, cond db.Cond) bool {
	return Datastore.EditByCond("bot_users", key, value, cond)
}
func BotUsersGetAll() ([]BotUser, bool) {
	var items []BotUser
	ok := Datastore.GetAll("bot_users", &items)
	return items, ok
}
func BotUsersGetsByLastAd(userID, limit int) ([]BotUser, bool) {
	var i []BotUser
	ok := Datastore.GetByCondOrderLimit("bot_users", &i, db.Cond{"user_id": userID, "is_active": true}, "last_msg", limit)
	return i, ok
}

//func BotUsersCount() (int, bool) {
//	count, ok := Datastore.Count("bot_users")
//	return count, ok
//}

func BotUsersCount(id int) (int, bool) {
	count, ok := Datastore.CountByCond("bot_users", db.Cond{"bot_id": id})
	return count, ok
}
func BotUsersActiveCount(id int) (int, bool) {
	count, ok := Datastore.CountByCond("bot_users", db.Cond{"bot_id": id, "is_active": true})
	return count, ok
}
func BotUsersOverallCount() (int, bool) {
	return Datastore.Count("bot_users")
}
func BotUsersOverallActiveCount() (int, bool) {
	return Datastore.CountByCond("bot_users", db.Cond{"is_active": true})
}
func (i *BotUser) IsEmpty() bool { return reflect.DeepEqual(BotUser{}, i) }
