package datastore

import (
	"reflect"
)

func StatAdd(s Stat) (int, bool) {
	return Datastore.Add("stats", s)
}

func StatGetBy(key, value interface{}) (Stat, bool) {
	var stat Stat
	ok := Datastore.GetBy("stats", &stat, key, value)
	return stat, ok
}
func StatGetLast() (Stat, bool) {
	var stat Stat
	ok := Datastore.GetLast("stats", &stat)
	return stat, ok
}

func StatGetByColumnOne(key, value, column, i interface{}) bool {
	return Datastore.GetByColumnOne("stats", key, value, column, i)
}
func StatsGetsBy(key, value interface{}) ([]Stat, bool) {
	var stats []Stat
	ok := Datastore.GetsBy("stats", &stats, key, value)
	return stats, ok
}

func StatEdit(s Stat) bool {
	_, ok := StatGetBy("id", s.ID)
	if ok {
		return Datastore.Edit("stats", "id", s.ID, s)
	}
	return false
}

func StatEditOne(key, value, field, i interface{}) bool {
	return Datastore.EditOne("stats", key, value, field, i)
}
func StatsGetAll() ([]Stat, bool) {
	var stats []Stat
	ok := Datastore.GetAll("stats", stats)
	return stats, ok
}

//func StatDeleteBy(key, value interface{}) {
//	Datastore.DeleteBy("stats", key, value)
//}
func (s *Stat) IsEmpty() bool { return reflect.DeepEqual(Stat{}, s) }
